﻿using System;
using System.Threading;

namespace Tasca2Threads
{
    class Program
    {

        static string[] firstPhrase = ("Una vegada hi havia un gat").Split(" ");
        static string[] secondPhrase = ("En un lugar habia una Mancha").Split(" ");
        static string[] thirdPhrase = ("Once upon a time in the westcoast").Split(" ");
        static void Main()
        {
            PhraseThreads();
            FridgeThreads();

        }

        static void PhraseThreads()
        {
            Thread FirstFile = new Thread(PrintWordsWithTimeOut);
            FirstFile.Start(firstPhrase);
            FirstFile.Join();
            Thread SecondFile = new Thread(PrintWordsWithTimeOut);
            SecondFile.Start(secondPhrase);
            SecondFile.Join();
            Thread ThirdFile = new Thread(PrintWordsWithTimeOut);
            ThirdFile.Start(thirdPhrase);
            ThirdFile.Join();
        }


        static void PrintWordsWithTimeOut(Object arrayObj)
        {
            string[] array = (string[])arrayObj;

            foreach (var word in array)
            {
                Console.Write(word + " ");
                Thread.Sleep(2000);
            }
            Console.WriteLine();

        }


        static void FridgeThreads()
        {
            Nevera nevera = new Nevera();

            Thread thread1 = new Thread(() =>
            {
                nevera.DrinkNBeers("Dr Dre");
            });
            Thread thread2 = new Thread(() =>
            {
                thread1.Join();
                nevera.FillFridgeWithBeers("Felip VI");
            });
            Thread thread3 = new Thread(() =>
            {
                thread2.Join();
                nevera.DrinkNBeers("Lauryn Hill");
            });
            Thread thread4 = new Thread(() =>
            {
                thread3.Join();
                nevera.DrinkNBeers("Aretha Franklin");
            });

            thread1.Start();
            thread2.Start();
            thread3.Start();
            thread4.Start();

        }

    }
}
