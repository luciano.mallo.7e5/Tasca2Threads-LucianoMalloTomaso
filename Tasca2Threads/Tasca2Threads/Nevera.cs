﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tasca2Threads
{
    class Nevera
    {
        private static int _totalBeers;
        Random random = new Random();
        private string _lastActionMadeBy;
        public Nevera()
        {
            _totalBeers = 6;
        }



        public void FillFridgeWithBeers(string name)
        {
            _lastActionMadeBy = name;
            
            int newRandomInt = random.Next(0, 7);
            if (_totalBeers + newRandomInt < 9)
                _totalBeers += newRandomInt;
            else
                _totalBeers = 9;

            Console.WriteLine("\n" + name + " ha llanado la nevera con " + newRandomInt + " cervezas");
            Console.WriteLine("Hay " + _totalBeers + " cervezas");
        }

        public void DrinkNBeers(string name)
        {
            _lastActionMadeBy = name;
            int newRandomInt = random.Next(0, 7);
            if (_totalBeers - newRandomInt > 0) { 
                _totalBeers -= newRandomInt;
            Console.WriteLine("\n" + name + " ha bebido " + newRandomInt + " cervezas"); 
            }
            else
            {
                _totalBeers = 0;
                Console.WriteLine("\n" + name + " ha bebido " + _totalBeers + " cervezas");
            }
            
            
            Console.WriteLine("Hay "+ _totalBeers + " cervezas");
        }

    }
}
/*
 un constructor
dues funcions
omplir nevera (que omple de 0 a 6 cerveses aleatòriament) a la nevera poden haver-hi màxim 9 cerveses.
beure cervesa (que resta de 0 a 6 cerveses aleatòriament)
a aquests dos mètodes se’ls ha de passar un nom (string) de la persona que fa l’acció.
mètode main on es crea una nevera de 6 cerveses, i, mitjançant threads, fem que:
Anitta ompli la nevera
Després Bad Bunny  begui
Després Lil Nas X begui
Després Manuel Turizo begui

 */